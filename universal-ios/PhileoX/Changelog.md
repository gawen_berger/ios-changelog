# Changelog Phileo X
![APP LOGO](/Users/gawenberger/Work/ccei/universal_ios/universal/fastlane/../ressources/PhileoX/app.xcassets/AppIcon.appiconset/87.png)

All versions delivered, whether it's on [AppCenter](https://appcenter.ms/orgs/CCEI/apps/Phileo-X) or [AppStoreConnect](https://appstoreconnect.apple.com/apps/1539633008/appstore/ios/version/inflight), will be documented in this file.
# Releases
#### 1.0.x Releases
- `1.0.1` Releases - [1.0.1 (28)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/1.0/1.0.1/1.0.1-28.md) 
- `1.0.0` Releases - [1.0.0 (27)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/1.0/1.0.0/1.0.0-27.md) | [1.0.0 (26)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/1.0/1.0.0/1.0.0-26.md) 

#### 0.5.x Releases
- `0.5.4` Releases - [0.5.4 (25)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.4/0.5.4-25.md) 
- `0.5.3` Releases - [0.5.3 (24)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.3/0.5.3-24.md) 
- `0.5.2` Releases - [0.5.2 (23)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.2/0.5.2-23.md) 
- `0.5.1` Releases - [0.5.1 (22)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.1/0.5.1-22.md) | [0.5.1 (21)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.1/0.5.1-21.md) | [0.5.1 (20)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.1/0.5.1-20.md) 
- `0.5.0` Releases - [0.5.0 (19)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.5/0.5.0/0.5.0-19.md) 

#### 0.4.x Releases
- `0.4.3` Releases - [0.4.3 (18)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.4/0.4.3/0.4.3-18.md) | [0.4.3 (17)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.4/0.4.3/0.4.3-17.md) 
- `0.4.2` Releases - [0.4.2 (16)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.4/0.4.2/0.4.2-16.md) | [0.4.2 (15)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.4/0.4.2/0.4.2-15.md) 
- `0.4.1` Releases - [0.4.1 (14)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.4/0.4.1/0.4.1-14.md) 
- `0.4.0` Releases - [0.4.0 (13)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.4/0.4.0/0.4.0-13.md) 

#### 0.3.x Releases
- `0.3.3` Releases - [0.3.3 (12)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.3/0.3.3/0.3.3-12.md) | [0.3.3 (11)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.3/0.3.3/0.3.3-11.md) | [0.3.3 (10)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.3/0.3.3/0.3.3-10.md) 
- `0.3.2` Releases - [0.3.2 (9)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.3/0.3.2/0.3.2-9.md) 
- `0.3.1` Releases - [0.3.1 (8)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.3/0.3.1/0.3.1-8.md) 
- `0.3.0` Releases - [0.3.0 (7)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.3/0.3.0/0.3.0-7.md) 

#### 0.2.x Releases
- `0.2.2` Releases - [0.2.2 (6)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.2/0.2.2/0.2.2-6.md) 
- `0.2.1` Releases - [0.2.1 (5)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.2/0.2.1/0.2.1-5.md) | [0.2.1 (4)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.2/0.2.1/0.2.1-4.md) 
- `0.2.0` Releases - [0.2.0 (3)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.2/0.2.0/0.2.0-3.md) 

#### 0.1.x Releases
- `0.1.0` Releases - [0.1.0 (2)](https://bitbucket.org/gawen_berger/ios-changelog/src/master/universal-ios/PhileoX/0.1/0.1.0/0.1.0-2.md) 


---