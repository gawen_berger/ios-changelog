# Phileo X 0.5.4 (25)
![APP LOGO](../ressources/PhileoX/app.xcassets/AppIcon.appiconset/87.png)
###### _11-05-2021_ - [Download link](https://install.appcenter.ms/orgs/CCEI/apps/Phileo-X/releases/30)
## Features
- add app version label in settings screen
- we can now copy the serial number by pressing on it for a bit more than a second

## Bug fixes
- name couldn't be edited if it was too long
- problem to retrieve the phone temperature unit

## Utils
- add extension to Bundle to retrieve app version and build number

---