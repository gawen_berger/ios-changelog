# Phileo X 0.5.1 (22)
![APP LOGO](../ressources/PhileoX/app.xcassets/AppIcon.appiconset/87.png)
###### _22-04-2021_ - [Download link](https://install.appcenter.ms/orgs/CCEI/apps/Phileo-X/releases/27)
## Bug fixes
- update firmware UI bugs
- __Fastlane :__ changelog generation

---